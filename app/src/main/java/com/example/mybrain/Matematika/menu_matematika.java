package com.example.mybrain.Matematika;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mybrain.R;

public class menu_matematika extends Activity {

    Button un,latsoal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_matematika);
        initialize();
    }

    private void initialize() {
        un = (Button) findViewById(R.id.btnUjianNasionalMatematika);
        latsoal = (Button) findViewById(R.id.btnLatihanSoalMatematika);

        un.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(menu_matematika.this,menuUN_Matematika.class);
                startActivity(intent);
            }
        });
        latsoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //belum
            }
        });
    }
    protected void onPause() {
        super.onPause();
        //lagu.release();
        finish();
    }
}
