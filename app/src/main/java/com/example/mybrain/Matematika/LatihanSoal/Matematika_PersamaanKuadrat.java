package com.example.mybrain.Matematika.LatihanSoal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.mybrain.HalamanNilai;
import com.example.mybrain.R;
import com.muddzdev.styleabletoastlibrary.StyleableToast;


public class Matematika_PersamaanKuadrat extends Activity implements View.OnClickListener {

    String kunci[] = {};

    int soal[] = {R.drawable.matematika_persamaan_kuadrat_no_1,R.drawable.matematika_persamaan_kuadrat_no_2,R.drawable.matematika_persamaan_kuadrat_no_3,
            R.drawable.matematika_persamaan_kuadrat_no_4,R.drawable.matematika_persamaan_kuadrat_no_5,R.drawable.matematika_persamaan_kuadrat_no_6,R.drawable.matematika_persamaan_kuadrat_no_7,
            R.drawable.matematika_persamaan_kuadrat_no_8,R.drawable.matematika_persamaan_kuadrat_no_9,R.drawable.matematika_persamaan_kuadrat_no_10,
            R.drawable.matematika_persamaan_kuadrat_no_11,R.drawable.matematika_persamaan_kuadrat_no_12,R.drawable.matematika_persamaan_kuadrat_no_13,
            R.drawable.matematika_persamaan_kuadrat_no_14,R.drawable.matematika_persamaan_kuadrat_no_15,R.drawable.matematika_persamaan_kuadrat_no_16,
            R.drawable.matematika_persamaan_kuadrat_no_17,R.drawable.matematika_persamaan_kuadrat_no_18,R.drawable.matematika_persamaan_kuadrat_no_19,
            R.drawable.matematika_persamaan_kuadrat_no_20};

    String Jawaban[] = new String[20];
    String answer;
    int i = 0;
    Button a,b,c,d;
    ImageView img;
    double Nilai,benar,salah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.un_mtk_2013);

        initialize();
        img.setImageResource(soal[i]);
        img.setMaxHeight(400);
        img.setMinimumHeight(400);
        img.setMinimumWidth(300);
        img.setMaxWidth(300);
        img.setPadding(80,10,80,510);

    }

    private double HitungNilai() {
        for(int j=0;j<soal.length;j++){
            if(kunci[j].equals(Jawaban[j])){
                benar++;
            }else{
                salah++;
            }
        }
        return benar*5;
    }

    private void initialize() {
        a = (Button) findViewById(R.id.btnA_PersamaanKuadrat);
        b = (Button) findViewById(R.id.btnB_PersamaanKuadrat);
        c = (Button) findViewById(R.id.btnC_PersamaanKuadrat);
        d = (Button) findViewById(R.id.btnD_PersamaanKuadrat);

        img = (ImageView) findViewById(R.id.soalPersamaanKuadrat);


        a.setOnClickListener(this);
        b.setOnClickListener(this);
        c.setOnClickListener(this);
        d.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnA_PersamaanKuadrat:
                answer = "A";
                if(i == 20){
                     Nilai = HitungNilai();
                     Intent transfer = new Intent(Matematika_PersamaanKuadrat.this, HalamanNilai.class);
                     Bundle b = new Bundle();
                     transfer.putExtra("pelajaran","MATEMATIKA : Persamaan Kuadrat");
                     b.putDouble("key", Nilai);
                     transfer.putExtras(b);
                     startActivity(transfer);
                }
                else if(answer.equals(kunci[i])&& (i <= 19)){
                    Jawaban[i] = answer;

                    if(i == 19){
                        i++;
                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Benar",R.style.truetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }else if(answer.equals(kunci[i]) == false && i <= 19){
                    Jawaban[i] = answer;
                    if(i == 19){
                        i++;

                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Salah",R.style.falsetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }
                break;
            case R.id.btnB_PersamaanKuadrat:
                answer = "B";

                if(i == 20){
                    Nilai = HitungNilai();
                    Intent transfer = new Intent(Matematika_PersamaanKuadrat.this, HalamanNilai.class);
                    Bundle b = new Bundle();
                    b.putDouble("key", Nilai);transfer.putExtra("pelajaran","MATEMATIKA : Persamaan Kuadrat");
                    transfer.putExtras(b);
                    startActivity(transfer);
                }
                else if(answer.equals(kunci[i])&& (i <= 19)){
                    Jawaban[i] = answer;

                    if(i == 19){
                        i++;
                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Benar",R.style.truetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }else if(answer.equals(kunci[i]) == false && i <= 19){
                    Jawaban[i] = answer;
                    if(i == 19){
                        i++;

                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Salah",R.style.falsetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }
                break;
            case R.id.btnC_PersamaanKuadrat:
                answer = "C";
                if(i == 20){
                    Nilai = HitungNilai();
                    Intent transfer = new Intent(Matematika_PersamaanKuadrat.this, HalamanNilai.class);
                    Bundle b = new Bundle();
                    b.putDouble("key", Nilai);transfer.putExtra("pelajaran","MATEMATIKA : Persamaan Kuadrat");
                    transfer.putExtras(b);
                    startActivity(transfer);
                }
                else if(answer.equals(kunci[i])&& (i <= 19)){
                    Jawaban[i] = answer;

                    if(i == 19){
                        i++;
                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Benar",R.style.truetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }else if(answer.equals(kunci[i]) == false && i <= 19){
                    Jawaban[i] = answer;
                    if(i == 39){
                        i++;

                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Salah",R.style.falsetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }
                break;
            case R.id.btnD_PersamaanKuadrat:
                answer = "D";

                if(i == 20){
                    Nilai = HitungNilai();
                    Intent transfer = new Intent(Matematika_PersamaanKuadrat.this, HalamanNilai.class);
                    Bundle b = new Bundle();
                    b.putDouble("key", Nilai);transfer.putExtra("pelajaran","MATEMATIKA : Persamaan Kuadrat");
                    transfer.putExtras(b);
                    startActivity(transfer);
                }
                else if(answer.equals(kunci[i])&& (i <= 19)){
                    Jawaban[i] = answer;

                    if(i == 19){
                        i++;
                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Benar",R.style.truetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }else if(answer.equals(kunci[i]) == false && i <= 19){
                    Jawaban[i] = answer;
                    if(i == 19){
                        i++;

                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Salah",R.style.falsetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }
                break;


        }
    }
    public void onBackPressed(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Matematika_PersamaanKuadrat.this);
        builder.setMessage("Yakin ingin mengakhiri mengerjakan soal?");
        builder.setCancelable(true);
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    protected void onPause() {
        super.onPause();
        //lagu.release();
        finish();
    }
}
