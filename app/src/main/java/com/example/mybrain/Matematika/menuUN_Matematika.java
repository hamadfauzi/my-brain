package com.example.mybrain.Matematika;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mybrain.R;

public class menuUN_Matematika extends Activity implements View.OnClickListener {

    Button un2017,un2016,un2015,un2014,un2013;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_un_matematika);

        initialize();

    }

    private void initialize() {
        un2013 = (Button) findViewById(R.id.btnUN_SMA_2013_MTK);
        un2014 = (Button) findViewById(R.id.btnUN_SMA_2014_MTK);
        un2015 = (Button) findViewById(R.id.btnUN_SMA_2015_MTK);
        un2016 = (Button) findViewById(R.id.btnUN_SMA_2016_MTK);
        un2017 = (Button) findViewById(R.id.btnUN_SMA_2017_MTK);

        un2013.setOnClickListener(this);
        un2014.setOnClickListener(this);
        un2015.setOnClickListener(this);
        un2016.setOnClickListener(this);
        un2017.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnUN_SMA_2013_MTK:
                Intent intent4 = new Intent(menuUN_Matematika.this,UN_MTK_2013.class);
                startActivity(intent4);
                break;
            case R.id.btnUN_SMA_2014_MTK:
                Intent intent3 = new Intent(menuUN_Matematika.this,UN_MTK_2014.class);
                startActivity(intent3);
                break;
            case R.id.btnUN_SMA_2015_MTK:
                Intent intent2 = new Intent(menuUN_Matematika.this,UN_MTK_2015.class);
                startActivity(intent2);
                break;
            case R.id.btnUN_SMA_2016_MTK:
                Intent intent1 = new Intent(menuUN_Matematika.this,UN_MTK_2016.class);
                startActivity(intent1);
                break;
            case R.id.btnUN_SMA_2017_MTK:
                Intent intent = new Intent(menuUN_Matematika.this,UN_MTK_2017.class);
                startActivity(intent);
                break;

        }
    }
    protected void onPause() {
        super.onPause();
        //lagu.release();
        finish();
    }
}
