package com.example.mybrain.Fisika;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mybrain.Matematika.menuUN_Matematika;
import com.example.mybrain.R;

public class menu_fisika extends Activity {

    Button un,latsoal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_fisika);
        initialize();
    }

    private void initialize() {
        un = (Button) findViewById(R.id.btnUjianNasionalFisika);
        latsoal = (Button) findViewById(R.id.btnLatihanSoalFisika);

        un.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(menu_fisika.this,menuUN_Fisika.class);
                startActivity(intent);
            }
        });
        latsoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //belum
            }
        });
    }
    protected void onPause() {
        super.onPause();
        //lagu.release();
        finish();
    }
}
