package com.example.mybrain.Biologi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mybrain.HalamanNilai;
import com.example.mybrain.R;
import com.muddzdev.styleabletoastlibrary.StyleableToast;


public class UN_BIOLOGI_2016 extends Activity implements View.OnClickListener {

    String kunci[] = {"C","C","D","C","E","E","A","C","E","A","A","B","C","E","E","D","B","A","B","C",
    "B","B","A","B","A","C","C","A","C","C","A","E","B","E","C","D","E","D","B","D"};

    int soal[] = {R.drawable.biologi_un_2016_no_1,R.drawable.biologi_un_2016_no_2,R.drawable.biologi_un_2016_no_3,
            R.drawable.biologi_un_2016_no_4,R.drawable.biologi_un_2016_no_5,R.drawable.biologi_un_2016_no_6,R.drawable.biologi_un_2016_no_7,
            R.drawable.biologi_un_2016_no_8,R.drawable.biologi_un_2016_no_9,R.drawable.biologi_un_2016_no_10,
            R.drawable.biologi_un_2016_no_11,R.drawable.biologi_un_2016_no_12,R.drawable.biologi_un_2016_no_13,
            R.drawable.biologi_un_2016_no_14,R.drawable.biologi_un_2016_no_15,R.drawable.biologi_un_2016_no_16,
            R.drawable.biologi_un_2016_no_17,R.drawable.biologi_un_2016_no_18,R.drawable.biologi_un_2016_no_19,
            R.drawable.biologi_un_2016_no_20,R.drawable.biologi_un_2016_no_21,R.drawable.biologi_un_2016_no_22,
            R.drawable.biologi_un_2016_no_23,R.drawable.biologi_un_2016_no_24,R.drawable.biologi_un_2016_no_25,
            R.drawable.biologi_un_2016_no_26,R.drawable.biologi_un_2016_no_27,R.drawable.biologi_un_2016_no_28,
            R.drawable.biologi_un_2016_no_29,R.drawable.biologi_un_2016_no_30,R.drawable.biologi_un_2016_no_31,
            R.drawable.biologi_un_2016_no_32,R.drawable.biologi_un_2016_no_33,R.drawable.biologi_un_2016_no_34,
            R.drawable.biologi_un_2016_no_35,R.drawable.biologi_un_2016_no_36,R.drawable.biologi_un_2016_no_37,
            R.drawable.biologi_un_2016_no_38,R.drawable.biologi_un_2016_no_39,R.drawable.biologi_un_2016_no_40};

    String Jawaban[] = new String[40];
    String answer;
    int i = 0;
    Button a,b,c,d,e;
    ImageView img;
    double Nilai,benar,salah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.un_biologi_2016);

        initialize();
        img.setImageResource(soal[i]);
        img.setMaxHeight(400);
        img.setMinimumHeight(400);
        img.setMinimumWidth(300);
        img.setMaxWidth(300);
        img.setPadding(80,10,80,510);

    }

    private double HitungNilai() {
        for(int j=0;j<soal.length;j++){
            if(kunci[j].equals(Jawaban[j])){
                benar++;
            }else{
                salah++;
            }
        }
        return benar*0.25;
    }

    private void initialize() {
        a = (Button) findViewById(R.id.btnA_UN_Biologi_2016);
        b = (Button) findViewById(R.id.btnB_UN_Biologi_2016);
        c = (Button) findViewById(R.id.btnC_UN_Biologi_2016);
        d = (Button) findViewById(R.id.btnD_UN_Biologi_2016);
        e = (Button) findViewById(R.id.btnE_UN_Biologi_2016);
        img = (ImageView) findViewById(R.id.soalBiologi2016);


        a.setOnClickListener(this);
        b.setOnClickListener(this);
        c.setOnClickListener(this);
        d.setOnClickListener(this);
        e.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnA_UN_Biologi_2016:
                answer = "A";
                if(i == 40){
                     Nilai = HitungNilai();
                     Intent transfer = new Intent(UN_BIOLOGI_2016.this, HalamanNilai.class);
                     Bundle b = new Bundle();
                     b.putDouble("key", Nilai);
                     transfer.putExtra("pelajaran", "BIOLOGI");
                     transfer.putExtras(b);
                     startActivity(transfer);
                }
                else if(answer.equals(kunci[i])&& (i <= 39)){
                    Jawaban[i] = answer;

                    if(i == 39){
                        i++;
                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Benar",R.style.truetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }else if(answer.equals(kunci[i]) == false && i <= 39){
                    Jawaban[i] = answer;
                    if(i == 39){
                        i++;

                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Salah",R.style.falsetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }
                break;
            case R.id.btnB_UN_Biologi_2016:
                answer = "B";

                if(i == 40){
                    Nilai = HitungNilai();
                    Intent transfer = new Intent(UN_BIOLOGI_2016.this, HalamanNilai.class);
                    Bundle b = new Bundle();
                    b.putDouble("key", Nilai);
                    transfer.putExtra("pelajaran", "BIOLOGI");

                    transfer.putExtras(b);
                    startActivity(transfer);
                }
                else if(answer.equals(kunci[i])&& (i <= 39)){
                    Jawaban[i] = answer;

                    if(i == 39){
                        i++;
                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Benar",R.style.truetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }else if(answer.equals(kunci[i]) == false && i <= 39){
                    Jawaban[i] = answer;
                    if(i == 39){
                        i++;

                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Salah",R.style.falsetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }
                break;
            case R.id.btnC_UN_Biologi_2016:
                answer = "C";
                if(i == 40){
                    Nilai = HitungNilai();
                    Intent transfer = new Intent(UN_BIOLOGI_2016.this, HalamanNilai.class);
                    Bundle b = new Bundle();
                    b.putDouble("key", Nilai);
                    transfer.putExtra("pelajaran", "BIOLOGI");

                    transfer.putExtras(b);
                    startActivity(transfer);
                }
                else if(answer.equals(kunci[i])&& (i <= 39)){
                    Jawaban[i] = answer;

                    if(i == 39){
                        i++;
                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Benar",R.style.truetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }else if(answer.equals(kunci[i]) == false && i <= 39){
                    Jawaban[i] = answer;
                    if(i == 39){
                        i++;

                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Salah",R.style.falsetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }
                break;
            case R.id.btnD_UN_Biologi_2016:
                answer = "D";

                if(i == 40){
                    Nilai = HitungNilai();
                    Intent transfer = new Intent(UN_BIOLOGI_2016.this, HalamanNilai.class);
                    Bundle b = new Bundle();
                    b.putDouble("key", Nilai);
                    transfer.putExtra("pelajaran", "BIOLOGI");

                    transfer.putExtras(b);
                    startActivity(transfer);
                }
                else if(answer.equals(kunci[i])&& (i <= 39)){
                    Jawaban[i] = answer;

                    if(i == 39){
                        i++;
                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Benar",R.style.truetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }else if(answer.equals(kunci[i]) == false && i <= 39){
                    Jawaban[i] = answer;
                    if(i == 39){
                        i++;

                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Salah",R.style.falsetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }
                break;
            case R.id.btnE_UN_Biologi_2016:
                answer = "E";

                if(i == 40){
                    Nilai = HitungNilai();
                    Intent transfer = new Intent(UN_BIOLOGI_2016.this, HalamanNilai.class);
                    Bundle b = new Bundle();
                    b.putDouble("key", Nilai);
                    transfer.putExtra("pelajaran", "BIOLOGI");

                    transfer.putExtras(b);
                    startActivity(transfer);
                }
                else if(answer.equals(kunci[i])&& (i <= 39)){
                    Jawaban[i] = answer;

                    if(i == 39){
                        i++;
                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Benar",R.style.truetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }else if(answer.equals(kunci[i]) == false && i <= 39){
                    Jawaban[i] = answer;
                    if(i == 39){
                        i++;

                        break;
                    }
                    i++;
                    StyleableToast.makeText(getBaseContext(),"Jawaban Anda Salah",R.style.falsetoast).show();
                    img.setImageResource(soal[i]);
                    img.setMaxHeight(400);
                    img.setMinimumHeight(400);
                    img.setMinimumWidth(300);
                    img.setMaxWidth(300);
                    img.setPadding(80,10,80,500);
                }
                break;

        }
    }
    public void onBackPressed(){
        AlertDialog.Builder builder = new AlertDialog.Builder(UN_BIOLOGI_2016.this);
        builder.setMessage("Yakin ingin mengakhiri mengerjakan soal?");
        builder.setCancelable(true);
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    protected void onPause() {
        super.onPause();
        //lagu.release();
        finish();
    }
}
