package com.example.mybrain.Biologi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mybrain.Fisika.UN_FISIKA_2013;
import com.example.mybrain.Fisika.UN_FISIKA_2014;
import com.example.mybrain.Fisika.UN_FISIKA_2015;
import com.example.mybrain.Fisika.UN_FISIKA_2016;
import com.example.mybrain.Fisika.UN_FISIKA_2017;
import com.example.mybrain.R;

public class menuUN_Biologi extends Activity implements View.OnClickListener {

    Button un2017,un2016,un2015,un2014,un2013;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_un_biologi);

        initialize();

    }

    private void initialize() {
        un2013 = (Button) findViewById(R.id.btnUN_SMA_2013_Biologi);
        un2014 = (Button) findViewById(R.id.btnUN_SMA_2014_Biologi);
        un2015 = (Button) findViewById(R.id.btnUN_SMA_2015_Biologi);
        un2016 = (Button) findViewById(R.id.btnUN_SMA_2016_Biologi);
        un2017 = (Button) findViewById(R.id.btnUN_SMA_2017_Biologi);

        un2013.setOnClickListener(this);
        un2014.setOnClickListener(this);
        un2015.setOnClickListener(this);
        un2016.setOnClickListener(this);
        un2017.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnUN_SMA_2013_Biologi:
                Intent intent4 = new Intent(menuUN_Biologi.this,UN_BIOLOGI_2013.class);
                startActivity(intent4);
                break;
            case R.id.btnUN_SMA_2014_Biologi:
                Intent intent3 = new Intent(menuUN_Biologi.this,UN_BIOLOGI_2014.class);
                startActivity(intent3);
                break;
            case R.id.btnUN_SMA_2015_Biologi:
                Intent intent2 = new Intent(menuUN_Biologi.this,UN_BIOLOGI_2015.class);
                startActivity(intent2);
                break;
            case R.id.btnUN_SMA_2016_Biologi:
                Intent intent1 = new Intent(menuUN_Biologi.this,UN_BIOLOGI_2016.class);
                startActivity(intent1);
                break;
            case R.id.btnUN_SMA_2017_Biologi:
                Intent intent = new Intent(menuUN_Biologi.this,UN_BIOLOGI_2017.class);
                startActivity(intent);
                break;

        }
    }
    protected void onPause() {
        super.onPause();
        //lagu.release();
        finish();
    }
}
