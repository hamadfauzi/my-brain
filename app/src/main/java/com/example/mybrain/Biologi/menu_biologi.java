package com.example.mybrain.Biologi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mybrain.Fisika.menuUN_Fisika;
import com.example.mybrain.R;

public class menu_biologi extends Activity {

    Button un,latsoal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_biologi);
        initialize();
    }

    private void initialize() {
        un = (Button) findViewById(R.id.btnUjianNasionalBiologi);
        latsoal = (Button) findViewById(R.id.btnLatihanSoalBiologi);

        un.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(menu_biologi.this,menuUN_Biologi.class);
                startActivity(intent);
            }
        });
        latsoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //belum
            }
        });
    }
    protected void onPause() {
        super.onPause();
        //lagu.release();
        finish();
    }
}
