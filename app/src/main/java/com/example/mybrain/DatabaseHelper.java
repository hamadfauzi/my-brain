package com.example.mybrain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "History.db";
   // public static final String TABLE_NAME = "Student_table";
    public static final String TABLE_PESERTA = "nilai_table";
    public static final String COL_NAME_PELAJARAN = "PELAJARAN";
    public static final String COL_NILAI = "NILAI";


    public static final String COL_1 = "ID";



    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //kode untuk membuat table student yang mempunyai kolom id , name , surnames , marks
        db.execSQL("create table "+TABLE_PESERTA+ " (ID INTEGER PRIMARY KEY AUTOINCREMENT,PELAJARAN TEXT,NILAI REAL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_PESERTA);
        onCreate(db);
    }
    public boolean insertDataNilai(String pelajaran, double nilai){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NAME_PELAJARAN,pelajaran);
        contentValues.put(COL_NILAI,nilai);

        long result = db.insert(TABLE_PESERTA,null,contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }
    public Cursor loadNilai(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_PESERTA,null);
        return res;
    }
    /*public boolean updateDataPeserta(String id, String name, String notelp, String alamat){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,id);
        contentValues.put(COL_NAME_PESERTA,name);
        contentValues.put(COL_NOTELP_PESERTA,notelp);
        contentValues.put(COL_ALAMAT_PESERTA,alamat);
        db.update(TABLE_PESERTA,contentValues,"id = ?",new String[]{id});
        return true;
    }*/
    /*public int deleteDataPeserta(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_PESERTA,"id = ?",new String[]{id});
    }*/
}
