package com.example.mybrain.Kimia;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mybrain.Fisika.menuUN_Fisika;
import com.example.mybrain.R;

public class menu_kimia extends Activity {

    Button un,latsoal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_kimia);
        initialize();
    }

    private void initialize() {
        un = (Button) findViewById(R.id.btnUjianNasionalKimia);
        latsoal = (Button) findViewById(R.id.btnLatihanSoalKimia);

        un.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(menu_kimia.this,menuUN_Kimia.class);
                startActivity(intent);
            }
        });
        latsoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //belum
            }
        });
    }
    protected void onPause() {
        super.onPause();
        //lagu.release();
        finish();
    }
}
