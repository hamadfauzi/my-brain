package com.example.mybrain;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class HalamanNilai extends Activity implements View.OnClickListener {
    Button share,back,simpan,buang;
    TextView score;
    DatabaseHelper MyDb;
    double result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.halaman_result);
        MyDb = new DatabaseHelper(this);
        Bundle b = getIntent().getExtras();
        result = b.getDouble("key");
        share = (Button) findViewById(R.id.btnShareNilai);
        back = (Button) findViewById(R.id.btnBackToMenu);
        simpan = (Button) findViewById(R.id.btnSimpanNilai);
        buang = (Button) findViewById(R.id.btnBuangNilai);
        score = (TextView) findViewById(R.id.txtNilaiAkhir);

        share.setOnClickListener(this);
        back.setOnClickListener(this);
        simpan.setOnClickListener(this);
        buang.setOnClickListener(this);
        score.setText(" "+result);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnBackToMenu:
                Intent intent = new Intent(HalamanNilai.this,menuAwal.class);
                startActivity(intent);
                break;
            case R.id.btnShareNilai:
                showDataNilai();
                break;
            case R.id.btnBuangNilai:

                break;
            case R.id.btnSimpanNilai:
                addDataNilai();
                break;
        }
    }

    private void showDataNilai() {

        Cursor res = MyDb.loadNilai();
        if(res.getCount() == 0){
            showMessage("Error","Data Not Found");
            return;
        }

        StringBuffer buffer = new StringBuffer();
        while(res.moveToNext()){
            buffer.append("No : "+res.getString(0)+"\n");
            buffer.append("Pelajaran : "+res.getString(1)+"\n");
            buffer.append("Nilai : "+res.getString(2)+"\n\n");
            showMessage("History Nilai",buffer.toString());
        }

    }

    public void showMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();

    }

    public void addDataNilai(){

        Bundle b = getIntent().getExtras();
        String kategori = b.getString("pelajaran");
        result = b.getDouble("key");
        boolean isInserted = MyDb.insertDataNilai(kategori,result);
        
            if(isInserted)
                Toast.makeText(getBaseContext(),"Nilai Berhasil Disimpan",Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getBaseContext(),"Gagal Menyimpan Nilai",Toast.LENGTH_SHORT).show();
    }
    protected void onPause() {
        super.onPause();
        //lagu.release();
        finish();
    }
}
