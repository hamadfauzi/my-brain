package com.example.mybrain;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mybrain.Biologi.*;
import com.example.mybrain.Fisika.*;
import com.example.mybrain.Kimia.menu_kimia;
import com.example.mybrain.Matematika.*;

public class menuAwal extends Activity implements View.OnClickListener {
    Button mtk,fisika,kimia,biologi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_awal);
        initialize();
    }

    private void initialize() {
        mtk = (Button) findViewById(R.id.btnMatematika);
        fisika = (Button) findViewById(R.id.btnFisika);
        kimia = (Button) findViewById(R.id.btnKimia);
        biologi = (Button) findViewById(R.id.btnBiologi);

        mtk.setOnClickListener(this);
        kimia.setOnClickListener(this);
        fisika.setOnClickListener(this);
        biologi.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){

            case R.id.btnBiologi:
                Intent intent = new Intent(menuAwal.this, menu_biologi.class);
                startActivity(intent);
                break;
            case R.id.btnFisika:
                Intent intent3 = new Intent(menuAwal.this, menu_fisika.class);
                startActivity(intent3);
                break;
            case R.id.btnKimia:
                Intent intent2 = new Intent(menuAwal.this, menu_kimia.class);
                startActivity(intent2);
                break;
            case R.id.btnMatematika:
                Intent intent1 = new Intent(menuAwal.this, menu_matematika.class);
                startActivity(intent1);
                break;
        }
    }
    protected void onPause() {
        super.onPause();
        //lagu.release();
        finish();
    }
}
